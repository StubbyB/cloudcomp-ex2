#!/bin/bash

INSTANCE_TYPE=$(jq -r .instance_type $1)
IMAGE_AMI=$(jq -r .image_ami $1)
REGION=$(jq -r .region $1)
OLD_REGION=$(aws configure get region)
aws configure set region ${REGION}
PORT=$(jq -r .port $1)
KEY_NAME=$(jq -r .key_name $1)
KEY_PEM="${KEY_NAME}.pem"
PROFILE_ARN=$(jq -r .iam_profile_arn $1)
SEC_GROUP_NAME=$(jq -r .security_group_name $1)
TARGET_GROUP_ARN=$(jq -r .target_group_arn $1)
SETTINGS_FILE_NAME=$1

echo "Creating another instance..."
RUN_INSTANCES=$(aws ec2 run-instances   \
    --count 1                           \
    --image-id ${IMAGE_AMI}        \
    --instance-type ${INSTANCE_TYPE}      \
    --key-name ${KEY_NAME}                \
    --iam-instance-profile Arn=${PROFILE_ARN} \
    --security-groups ${SEC_GROUP_NAME})

INSTANCE_IDS=$(echo ${RUN_INSTANCES} | jq -r '[.Instances[].InstanceId]|join(" ")')
INSTANCE_IDS_PREFIX=$(echo ${RUN_INSTANCES} | jq -r '.Instances[]|="Id="+.InstanceId|.Instances|join(" ")')

echo "Waiting for instance creation..."
aws ec2 wait instance-running --instance-ids ${INSTANCE_IDS}

PUBLIC_IPS=$(aws ec2 describe-instances --instance-ids ${INSTANCE_IDS} | \
    jq -r '[.Reservations[].Instances[].PublicIpAddress]|join(" ")' )

echo "New instance ${INSTANCE_IDS} @ ${PUBLIC_IPS}"

echo "Registering targets"
aws elbv2 register-targets \
    --target-group-arn ${TARGET_GROUP_ARN} \
    --targets ${INSTANCE_IDS_PREFIX}

echo "configuring and running EC2 instances"
for PUBLIC_IP in ${PUBLIC_IPS}
do
    ssh -i ${KEY_PEM} -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=10" ubuntu@${PUBLIC_IP} << EOF
        echo "{\"port\":8080,\"region\": \"${REGION}\", \"target_group_arn\": \"${TARGET_GROUP_ARN}\", \
        \"key_name\": \"${KEY_NAME}\", \"image_ami\": \"${IMAGE_AMI}\", \"instance_type\": \"${INSTANCE_TYPE}\", \
        \"iam_profile_arn\": \"${PROFILE_ARN}\", \
        \"security_group_name\": \"${SEC_GROUP_NAME}\"}"  > ${SETTINGS_FILE_NAME}
        sudo apt-get update
        sleep 3
        sudo apt-get update
        sudo apt-get install python3-pip -y
        sudo apt-get install python3-flask -y
        sudo apt-get install jq -y
        git clone https://gitlab.com/StubbyB/cloudcomp-ex2.git
        cd cloudcomp-ex2
        pip3 install -r requirements.txt
        ./run_node.sh ../${SETTINGS_FILE_NAME}
EOF

aws configure set region ${OLD_REGION}

echo "Done. new instance ${INSTANCE_IDS} @ ${PUBLIC_IPS}"
done
