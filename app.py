from flask import Flask, request, abort
from datacluster import Cluster
from datetime import datetime
import os

app = Flask(__name__)
app.app_context().push()

cluster = Cluster(local_port=os.environ.get('FLASK_RUN_PORT'))


@app.route('/alive', methods=['GET'])
def alive():
    return 'Yep'


@app.route('/key/<str_key>', methods=['GET'])
def get(str_key):
    data = cluster.get(str_key)
    if data:
        return data

    return 'Key not found', 404


@app.route('/key/<str_key>', methods=['PUT'])
def put(str_key):
    try:
        request_data = request.json
        expiration_date = datetime.strptime(request_data['expiration_date'], '%Y-%m-%dT%H:%M:%S')
        data = request_data['data']
        return cluster.put(str_key, data, expiration_date)
    except TimeoutError as e:
        return str(e), 500


@app.route('/backup/key/<str_key>', methods=['PUT'])
def put_backup(str_key):
    try:
        request_data = request.json
        expiration_date = datetime.strptime(request_data['expiration_date'], '%Y-%m-%dT%H:%M:%S')
        data = request_data['data']
        return cluster.put_backup(str_key, data, expiration_date)
    except TimeoutError as e:
        return str(e), 500


@app.route('/key/<str_key>', methods=['DELETE'])
def delete_local_data(str_key):
    data = cluster.delete_local_key(str_key)
    if data:
        return str(data)

    return 'Key not found', 404


@app.route('/data/node', methods=['GET'])
def get_node_data():
    if app.env != 'development':
        abort(404)
    else:
        return cluster.get_node_data()


@app.route('/data/cluster', methods=['GET'])
def get_cluster_data():
    if app.env != 'development':
        abort(404)
    else:
        return cluster.get_cluster_data()
