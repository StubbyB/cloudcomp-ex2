#!/bin/bash

# REGION='us-east-1'
# IMAGE_AMI="ami-09e67e426f25ce0d7"
# INSTANCE_TYPE='t2.micro'

REGION='eu-north-1'
IMAGE_AMI="ami-0ff338189efb7ed37"
INSTANCE_TYPE='t3.micro'

OLD_REGION=$(aws configure get region)
aws configure set region ${REGION}
AWS_ACCOUNT=$(aws sts get-caller-identity  | jq -r .Account)
RUN_ID=$(date +'%N')
VPC=$(aws ec2 describe-subnets | jq -r ".Subnets[0].VpcId")
SUBNETS=$(aws ec2 describe-subnets | jq -r '[.Subnets[].SubnetId]|join(" ")')
TARGET_GROUP_NAME="target-group-${RUN_ID}"
SEC_GROUP_NAME="security-group-${RUN_ID}"
ELB_NAME="elb-${RUN_ID}"
EC2_ROLE="ec2-role-$RUN_ID"
ROLE_POLICY="ec2-elb-read-policy-${RUN_ID}"
INSTANCE_PROFILE_NAME="instance-profile-${RUN_ID}"
SETTINGS_FILE_NAME="settings-${RUN_ID}.json"

KEY_NAME="key-${RUN_ID}"
KEY_PEM="${KEY_NAME}.pem"
echo "create key pair $KEY_PEM to connect to instances and save locally"
aws ec2 create-key-pair --key-name ${KEY_NAME} \
    | jq -r ".KeyMaterial" > ${KEY_PEM}

chmod 400 ${KEY_PEM}

echo "Creating security group ${SEC_GROUP_NAME}"
SEC_GROUP_ID=$(aws ec2 create-security-group --group-name ${SEC_GROUP_NAME} --description yep | jq -r .GroupId)

MY_IP=$(curl ipinfo.io/ip)
echo "My IP: $MY_IP"

aws ec2 authorize-security-group-ingress        \
    --group-name ${SEC_GROUP_NAME} --port 22 --protocol tcp \
    --cidr ${MY_IP}/32

aws ec2 authorize-security-group-ingress        \
    --group-name ${SEC_GROUP_NAME} --port 8080 --protocol tcp \
    --cidr 0.0.0.0/0

echo "Creating role ${EC2_ROLE}..."
aws iam create-role --role-name ${EC2_ROLE} --assume-role-policy-document file://trust-policy.json

echo "Putting inline policy in role..."
aws iam put-role-policy --role-name ${EC2_ROLE} --policy-name ${ROLE_POLICY} \
--policy-document=file://role-inline-policy.json


echo "Wait for role creation"

aws iam wait role-exists --role-name ${EC2_ROLE}
aws iam get-role --role-name ${EC2_ROLE}
AWS_ROLE_ARN=$(aws iam get-role --role-name ${EC2_ROLE} | jq -r .Role.Arn)

echo "Workaround consistency rules in AWS roles after creation... (sleep 10)"
sleep 10

echo "Creating instance profile ${INSTANCE_PROFILE_NAME} and attaching role ${EC2_ROLE}"
PROFILE_ARN=$(aws iam create-instance-profile --instance-profile-name ${INSTANCE_PROFILE_NAME} | \
    jq -r '.InstanceProfile.Arn')
aws iam wait instance-profile-exists --instance-profile-name ${INSTANCE_PROFILE_NAME}
aws iam add-role-to-instance-profile --instance-profile-name ${INSTANCE_PROFILE_NAME} --role-name ${EC2_ROLE}

echo "Workaround consistency rules in AWS instance profiles after creation... (sleep 10)"
sleep 10

echo "Creating instances..."
RUN_INSTANCES=$(aws ec2 run-instances   \
    --count 3                           \
    --image-id ${IMAGE_AMI}        \
    --instance-type ${INSTANCE_TYPE}      \
    --key-name ${KEY_NAME}                \
    --iam-instance-profile Arn=${PROFILE_ARN} \
    --security-groups ${SEC_GROUP_NAME})

INSTANCE_IDS=$(echo ${RUN_INSTANCES} | jq -r '[.Instances[].InstanceId]|join(" ")')
INSTANCE_IDS_PREFIX=$(echo ${RUN_INSTANCES} | jq -r '.Instances[]|="Id="+.InstanceId|.Instances|join(" ")')

echo "Waiting for instance creation..."
aws ec2 wait instance-running --instance-ids ${INSTANCE_IDS}

PUBLIC_IPS=$(aws ec2 describe-instances --instance-ids ${INSTANCE_IDS} | \
    jq -r '[.Reservations[].Instances[].PublicIpAddress]|join(" ")' )

echo "New instances ${INSTANCE_IDS} @ ${PUBLIC_IPS}"

echo "Creating target group ${TARGET_GROUP_NAME}"
TARGET_GROUP_ARN=$(aws elbv2 create-target-group --name ${TARGET_GROUP_NAME} --protocol HTTP --port 8080 \
    --target-type instance --vpc-id ${VPC} --health-check-protocol HTTP --health-check-path '/alive' \
    --health-check-interval-seconds 5 --health-check-timeout-seconds 3 --healthy-threshold-count 3 \
    --unhealthy-threshold-count 2 | jq -r .TargetGroups[0].TargetGroupArn)

echo "Registering targets"
aws elbv2 register-targets \
    --target-group-arn ${TARGET_GROUP_ARN} \
    --targets ${INSTANCE_IDS_PREFIX}

echo "creating load balancer ${ELB_NAME}"
ELB_ARN=$(aws elbv2 create-load-balancer --name ${ELB_NAME} \
--subnets ${SUBNETS} --security-groups ${SEC_GROUP_ID} | jq -r .LoadBalancers[0].LoadBalancerArn)

echo "creating listener"
T=$(aws elbv2 create-listener \
    --load-balancer-arn ${ELB_ARN} \
    --protocol HTTP \
    --port 8080 \
    --default-actions Type=forward,TargetGroupArn=${TARGET_GROUP_ARN})

ELB_DNS=$(aws elbv2 describe-load-balancers --load-balancer-arns ${ELB_ARN} | jq -r .LoadBalancers[0].DNSName)

echo "created ELB at ${ELB_DNS}"

echo "{'port': 8080, 'region': ${REGION}, 'target_group_arn': ${TARGET_GROUP_ARN}}" > settings-${RUN_ID}.json
echo "{\"port\":8080,\"region\": \"${REGION}\", \"target_group_arn\": \"${TARGET_GROUP_ARN}\", \
        \"key_name\": \"${KEY_NAME}\", \"image_ami\": \"${IMAGE_AMI}\", \"instance_type\": \"${INSTANCE_TYPE}\", \
        \"iam_profile_arn\": \"${PROFILE_ARN}\", \
        \"security_group_name\": \"${SEC_GROUP_NAME}\"}"  > ${SETTINGS_FILE_NAME}
echo "configuring and running EC2 instances"
for PUBLIC_IP in ${PUBLIC_IPS}
do
    ssh -i ${KEY_PEM} -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=10" ubuntu@${PUBLIC_IP} << EOF
        echo "{\"port\":8080,\"region\": \"${REGION}\", \"target_group_arn\": \"${TARGET_GROUP_ARN}\", \
        \"key_name\": \"${KEY_NAME}\", \"image_ami\": \"${IMAGE_AMI}\", \"instance_type\": \"${INSTANCE_TYPE}\", \
        \"iam_profile_arn\": \"${PROFILE_ARN}\", \
        \"security_group_name\": \"${SEC_GROUP_NAME}\"}"  > ${SETTINGS_FILE_NAME}
        sudo apt-get update
        sleep 3
        sudo apt-get update
        sudo apt-get install python3-pip -y
        sudo apt-get install python3-flask -y
        sudo apt-get install jq -y
        git clone https://gitlab.com/StubbyB/cloudcomp-ex2.git
        cd cloudcomp-ex2
        pip3 install -r requirements.txt
        ./run_node.sh ../${SETTINGS_FILE_NAME}
EOF

aws configure set region ${OLD_REGION}

echo "Done. created ELB at ${ELB_DNS}"
echo "New instances ${INSTANCE_IDS} @ ${PUBLIC_IPS}"
echo "created a settings file ${SETTINGS_FILE_NAME} on this machine and each instance, used for running and creating nore instances"
done
