import socket
import os
import boto3

ec2_client = boto3.client('ec2', os.environ['AWS_REGION'])
elb_client = boto3.client('elbv2', os.environ['AWS_REGION'])
target_group_arn = os.environ['AWS_TARGET_GROUP_ARN']


def get_private_ip():
    return socket.gethostbyname(socket.gethostname())


def discover_available_nodes():
    instance_ports = {x['Target']['Id']: x['Target']['Port'] for x in
                      elb_client.describe_target_health(TargetGroupArn=target_group_arn)['TargetHealthDescriptions']
                      if x['TargetHealth']['State'] == 'healthy'}
    instance_ids = list(instance_ports.keys())
    instance_ip = {x['InstanceId']: x.get('PrivateIpAddress', None) for res in ec2_client.describe_instances(Filters=[
        {'Name': 'instance-id', 'Values': instance_ids}])['Reservations'] for x in res['Instances']}

    return set(['{0}:{1}'.format(y, instance_ports[x]) for x, y in instance_ip.items()])
