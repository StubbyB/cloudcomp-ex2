#!/bin/bash
export FLASK_RUN_PORT=$(jq -r '.port' $1)
export FLASK_APP=app.py
export FLASK_ENV=development
export AWS_REGION=$(jq -r '.region' $1)
export AWS_TARGET_GROUP_ARN=$(jq -r '.target_group_arn' $1)
nohup flask run --host 0.0.0.0  &>/dev/null &