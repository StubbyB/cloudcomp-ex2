from uhashring import HashRing
from apscheduler.schedulers.background import BackgroundScheduler
import network_utils
from datacluster.cluster_local_node import ClusterLocalNode
from datacluster.cluster_remote_node import ClusterRemoteNode


class Cluster:
    def __init__(self, local_port):
        self.host = network_utils.get_private_ip()
        self.qualified_host = f'{self.host}:{local_port}'
        self.ring = HashRing()
        self.node = ClusterLocalNode(self.host, local_port)
        self.excluding_ring = None
        self.ring.add_node(self.node.qualified_host, {'hostname': self.host, 'port': local_port, 'instance': self.node})

        self.__create_excluding_ring()
        self.__update_ring()

        scheduler = BackgroundScheduler()
        scheduler.add_job(func=self.__update_ring, trigger="interval", seconds=5)
        scheduler.start()

    def get(self, str_key):
        return self.node.get(str_key) or self.ring.get_node_instance(str_key).get(str_key)

    def put(self, str_key, data, expiration_date):
        node = self.ring.get_node_instance(str_key)
        backup_node = None
        if node.qualified_host == self.qualified_host:
            backup_node = self.__store_in_backup(str_key, data, expiration_date)

        self.__store_in_primary_node(str_key, data, expiration_date, node, backup_node)

        return 'Yay!'

    def put_backup(self, str_key, data, expiration_date):
        self.node.put(str_key, data, expiration_date, self.ring.get_node(str_key), True)
        return 'Yay!'

    def delete_local_key(self, str_key):
        return self.node.delete_local_key(str_key)

    def __delete_from_remote(self, str_key, node_id):
        try:
            node = self.ring.nodes[node_id]['instance']
            node.delete_local_key(str_key)
        except Exception as e:
            raise TimeoutError('Timeout occurred while deleting from remote node, please try again later.') from e

    @staticmethod
    def __store_in_primary_node(str_key, data, expiration_date, node, backup_node=None):
        try:
            host = backup_node.qualified_host if backup_node else None
            node.put(str_key, data, expiration_date, node.qualified_host, False, host)
        except Exception as e:
            raise TimeoutError('Timeout occurred while storing in remote node, please try again later.') from e

    def __store_in_backup(self, str_key, data, expiration_date):
        try:
            backup_node = self.excluding_ring.get_node_instance(str_key)
            backup_node.put(str_key, data, expiration_date, self.node.qualified_host, True)
            if str_key in self.node.data:
                self.node.data[str_key]['backup_node'] = backup_node.qualified_host
            return backup_node
        except Exception as e: # TODO: narrow down, better handling
            raise TimeoutError('Timeout occurred during backup, please try again later.') from e

    def __update_ring(self):
        nodes = network_utils.discover_available_nodes()
        fallen_nodes = self.ring.get_nodes() - nodes - {self.qualified_host}
        new_nodes = nodes - self.ring.get_nodes()

        self.__remove_fallen_nodes(fallen_nodes)
        self.__add_new_nodes(new_nodes)

    def __create_excluding_ring(self):
        new_ring = HashRing(self.ring.nodes)
        new_ring.remove_node(self.qualified_host)
        self.excluding_ring = new_ring

# new nodes
# transfer data to new primaries/backups, delete from old backups if needed

    def __add_new_nodes(self, nodes):
        for node in nodes:
            cluster_node = ClusterRemoteNode(*node.split(':'))
            self.ring.add_node(cluster_node.qualified_host, {'hostname': cluster_node.host, 'port': cluster_node.port,
                                                             'instance': cluster_node})
        self.__create_excluding_ring()
        mock_ring = HashRing(self.ring.nodes)
        keys_to_pop = []
        for key, record in self.node.data.items():
            key_node = self.ring.get_node_instance(key)
            backup_key_node = self.__get_actual_backup_node(key, key_node, mock_ring)

            if not record['is_backup']:
                if key_node.qualified_host != self.node.qualified_host:  # i'm no longer primary
                    # send to new primary and delete from backup and local
                    self.__store_in_primary_node(key, record['data'], key_node)
                    if backup_key_node.qualified_host != record['backup_node']:  # backup changed, delete from old
                        self.__delete_from_remote(key, record['backup_node'])
                    if backup_key_node.qualified_host != self.node.qualified_host:  # if i'm not backup, remove
                        keys_to_pop.append( key)
                elif backup_key_node.qualified_host != record['backup_node']:  # backup changed? idk if possible
                    # save in new backup and delete from old
                    backup_node = record['backup_node']
                    self.__store_in_backup(key, record['data'])
                    self.__delete_from_remote(key, backup_node)

        for key in keys_to_pop:
            self.node.data.pop(key)

    def __get_actual_backup_node(self, key, key_node, mock_ring):
        tmp_node = mock_ring.nodes[key_node.qualified_host]
        mock_ring.remove_node(key_node.qualified_host)
        backup_key_node = mock_ring.get_node_instance(key)
        mock_ring.add_node(key_node.qualified_host, tmp_node)
        return backup_key_node

    # dead nodes

    def __remove_fallen_nodes(self, nodes):
        claimed_records = self.__claim_keys_and_get_backups(nodes)

        for node in nodes:
            self.ring.remove_node(node)

        self.__create_excluding_ring()

        for record in claimed_records:
            self.__store_in_backup(*record)

    def __claim_keys_and_get_backups(self, nodes):
        keys_for_backup = []
        mock_ring = HashRing(self.ring.nodes)

        for key, record in self.node.data.items():
            if record['origin'] in nodes and record['is_backup']:  # i'm backup, origin died, now i'm origin
                node_id = record['origin']
                tmp_node = mock_ring.nodes[node_id]
                mock_ring.remove_node(node_id)
                if mock_ring.get_node(key) == self.node.qualified_host:  # making sure that i'm the new origin, whatever
                    record['origin'] = self.node.qualified_host
                    record['is_backup'] = False
                    keys_for_backup.append((key, record['data']))
                mock_ring.add_node(node_id, tmp_node)
            if not record['is_backup'] and record['backup_node'] in nodes:  # the backup is dead, backup in the new one
                keys_for_backup.append((key, record['data']))
        return set(keys_for_backup)

# all data
    def get_node_data(self):
        return self.node.data

    def get_cluster_data(self):
        result = {}
        for node in self.ring.get_instances():
            result[node.qualified_host] = node.get_all_data()

        return result
