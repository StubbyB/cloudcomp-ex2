from abc import ABC, abstractmethod


class ClusterNode(ABC):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.qualified_host = f'{host}:{port}'

    @abstractmethod
    def put(self, str_key, data, expiration_date, origin, is_backup, backup_node=None):
        pass

    @abstractmethod
    def get(self, str_key):
        pass

    @abstractmethod
    def delete_local_key(self, str_key):
        pass

    @abstractmethod
    def get_all_data(self):
        pass
