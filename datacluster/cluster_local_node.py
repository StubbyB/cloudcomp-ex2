from datacluster.cluster_node import ClusterNode


class ClusterLocalNode(ClusterNode):
    def __init__(self, host, port):
        super().__init__(host, port)
        self.data = {}

    def put(self, str_key, data, expiration_date, origin, is_backup, backup_node=None):
        self.data[str_key] = {
            'key': str_key,
            'is_backup': is_backup,
            'data': data,
            'expiration_date':  expiration_date.strftime('%Y-%m-%dT%H:%M:%S'),
            'origin': origin,
            'backup_node': backup_node
        }
        print(f'Stored! key: {str_key}, is_backup: {is_backup}, origin: {origin}')

    def get(self, str_key):
        result = self.data.get(str_key, None)
        return (result or None) and result['data']

    def delete_local_key(self, str_key):
        return self.data.pop(str_key, None) is not None

    def get_all_data(self):
        return self.data
