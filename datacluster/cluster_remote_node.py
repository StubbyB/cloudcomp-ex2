import requests
from requests.adapters import HTTPAdapter
from datacluster.cluster_node import ClusterNode


class ClusterRemoteNode(ClusterNode):
    def __init__(self, host, port):
        super().__init__(host, port)
        self.session = requests.Session()
        self.session.mount('http://', HTTPAdapter(max_retries=0))

    # ignoring origin on remote for now, self determined, maybe transmit that in the future
    def put(self, str_key, data, expiration_date, origin, is_backup, backup_node=None):
        prefix = 'backup/key' if is_backup else 'key'
        body = {
            'data': data,
            'expiration_date': expiration_date.strftime('%Y-%m-%dT%H:%M:%S'),
        }
        self.session.put(f'http://{self.qualified_host}/{prefix}/{str_key}', json=body)

        print(f'Stored in remote!! key: {str_key}, is_backup: {is_backup}, remote: {self.qualified_host}')

    def get(self, str_key):
        result = self.session.get(f'http://{self.qualified_host}/key/{str_key}')
        return result.content if result.status_code == 200 else None

    def delete_local_key(self, str_key):
        result = self.session.delete(f'http://{self.qualified_host}/key/{str_key}')
        return result.content == 'True' if result.status_code == 200 else None

    def get_all_data(self):
        result = self.session.get(f'http://{self.qualified_host}/data/node')
        return result.json() if result.status_code == 200 else None
