# CloudComp Ex2

```bash
git clone https://gitlab.com/StubbyB/cloudcomp-ex2.git
cd cloudcomp-ex2
./init.sh # some installations, if needed
./deploy.sh # assuming aws cli has been configured previously
```

Notice that the default region is `eu-north-1`, as the `IMAGE_AMI` differs between regions.
If you wish to create the cluster a different region, modify the constants at the top of `deploy.sh`.

After deploying, a settings file will be created.
If you wish to create another node and connected it to the cluster, you will need to use the created file:

```bash
./extra_node.sh <SETTINGS_FILE> # e.g. ./extra_node.sh settings-923755687.json
```

The API is accessible through port `8080`, and supports the following operations:
```bash
PUT /key/<str_key>  -  body format: {'data': str, 'expiration_date': date('%Y-%m-%dT%H:%M:%S')}

Example:

curl --location --request PUT 'elb-12424124-34534534.eu-north-1.elb.amazonaws.com:8080/key/dummy_key' \
--header 'Content-Type: application/json' \
--data-raw '{
    "data": "dummy",
    "expiration_date": "2032-01-02T03:10:05"
}'
``` 
```
GET /key/<str_key>   -  returns the content
```
```
GET /data/cluster  -  for debugging, entire cluster data
```
```
GET /data/node  -  for debugging, the specific node's data
```


# Post submission edit:
The deploy script in this branch is setting up each instance one by one, allowing you to see everything that happens, but takes time.
I have created another branch with modified scripts which set up the nodes concurrently, saving quite a bit of time.

Didn't want to push to main after submission, so this message is the only change in this branch.
